package nz.govt.doc.t1m.services.credentials;

import com.bitbucket.thinbus.srp6.js.*;
import com.nimbusds.srp6.*;

import java.math.BigInteger;

/**
 * Created by PerfectR on 23/03/2016.
 */
public class AuthenticationControllerTest {

    public static SRP6CryptoParams getSrp6CryptoParams() {
        BigInteger n = new BigInteger("21766174458617435773191008891802753781907668374255538511144643224689886235383840957210909013086056401571399717235807266581649606472148410291413364152197364477180887395655483738115072677402235101762521901569820740293149529620419333266262073471054548368736039519702486226506248861060256971802984953561121442680157668000761429988222457090413873973970171927093992114751765168063614761119615476233422096442783117971236371647333871414335895773474667308967050807005509320424799678417036867928316761272274230314067548291133582479583061439577559347101961771406173684378522703483495337037655006751328447510550299250924469288819");
        BigInteger g = new BigInteger("2");
        String h = "SHA-256";
        SRP6CryptoParams cryptoParams = new SRP6CryptoParams(n, g, h);
        return cryptoParams;
    }

    /**
     *
     * @param username
     * @param password
     */
    public static void createNewRegistrationData(String username, String password) {
        SRP6CryptoParams cryptoParams = AuthenticationControllerTest.getSrp6CryptoParams();

        // Create verifier generator
        SRP6VerifierGenerator gen = new SRP6VerifierGenerator(cryptoParams);
        //gen.setXRoutine(new XRoutineWithUserIdentity());
        gen.setXRoutine(new HexHashedXRoutine());

        // Random 16 byte salt 's'
        //BigInteger salt = new BigInteger(SRP6VerifierGenerator.generateRandomSalt());
        //String saltHex = BigIntegerUtils.toHex(salt);
        String saltHex = "4173edea32a55437fed5309658254ff63d24b8a1a000722a1bfbc05d582842d8";
        BigInteger salt = BigIntegerUtils.fromHex(saltHex);

        // Compute verifier 'v'
        BigInteger verifier = gen.generateVerifier(salt, username, password);
        String verifierHex = BigIntegerUtils.toHex(verifier);


        String expectedSalt = "4173edea32a55437fed5309658254ff63d24b8a1a000722a1bfbc05d582842d8";
        String expectedVerifier = "93f82d8fa13156a4d48687f2aa77c44822616981f19d698ea21392179fa548e489e7e9d0e63c8351d422d991a4916419ab0c89b9390be9b23c36693577a31abf3daf5b38274f782278bda41fc3793de539ba07b1cb6c1e504a0df826b07e8274b6155e856de3c8e75b30be1ca0670d28c8adb0ac5e9c99feaabfffdbc3d23a06bee8d26e01302a27f506c42832991035e81fe76d013c8c9729c924e988e6b0f25b1008b2db75b662b3247458a2febc5c8588eefeaba69d84ff64984e9d38aa7a48801dd5c49ee9644c68058c7dccc2f3bf3183ff5b9a04cc7d7555b4016867ae2f0ec15da3c6dd08417f78e63211c8ad9ce58a59a181dc107f70df98270524ec";

        System.out.println("Salt:     " + saltHex);
        System.out.println("Exepcted: " + expectedSalt);

        System.out.println();

        System.out.println("Verifier: " + verifierHex);
        System.out.println("Expected: " + expectedVerifier);
    }

    public static void main(String[] args) throws Exception {

        String username = "Alicia.Rhodes";
        String password = "abc";

        AuthenticationControllerTest.createNewRegistrationData(username, password);


        System.exit(0);

        AuthenticationController authenticationController = new AuthenticationController();
        authenticationController.thinbusSRPService = new ThinbusSRPService();

        Step1Data step1DataRequest = new Step1Data();
        step1DataRequest.setUsername(username);

        SRP6CryptoParams cryptoParams = AuthenticationControllerTest.getSrp6CryptoParams();
        SRP6JavaClientSession client = new SRP6JavaClientSessionSHA256(cryptoParams.N.toString(), cryptoParams.g.toString());
        client.step1(username, password);

        Step1Data step1DataResponse = authenticationController.authenticateStep1(step1DataRequest).getBody();

        String salt = step1DataResponse.getSalt();
        String b = step1DataResponse.getB();
        SRP6ClientCredentials credentials = client.step2(salt, b);

        Step2Data step2Data = new Step2Data();
        step2Data.setA(BigIntegerUtils.toHex(credentials.A));
        step2Data.setM1(BigIntegerUtils.toHex(credentials.M1));

        Step2Data step2DataResponse = authenticationController.authenticateStep2(step2Data).getBody();

        String m2 = step2DataResponse.getM2();
        client.step3(m2);
    }
}
