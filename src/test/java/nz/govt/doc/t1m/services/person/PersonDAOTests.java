package nz.govt.doc.t1m.services.person;

import nz.govt.doc.t1m.Application;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import nz.govt.doc.t1m.domain.person.PersonGroup;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class PersonDAOTests {

    @Autowired
    protected PersonRepository personRepository;

    @Test
    public void whenSimpleCRUD() {
        PersonEntity p1 = new PersonEntity();
        p1.setFirstName("Mary");
        p1.setFamilyName("Smith" + System.currentTimeMillis());
        p1.setUsername(p1.getFirstName() + "." + p1.getFamilyName());
        p1.setPersonGroup(PersonGroup.SYS_ADMIN);

        //personDAO.save(p);
        PersonEntity p2 = personRepository.save(p1);
        PersonEntity p3 = personRepository.findOne(p2.getPersonId());

        assertThat(p1.getUsername(), equalTo(p3.getUsername()));

        PersonEntity p4 = personRepository.findOneByUsername(p1.getUsername());
        assertThat(p1.getUsername(), equalTo(p4.getUsername()));

        // One example of how to test that an exception is thrown, can also use the "expected" attribute of the @Test annotation
        // but wanted to do a logical delete in this same method.
        DataAccessException expectedException = null;
        try {
            PersonEntity p5 = new PersonEntity();
            p5.setUsername(p1.getUsername());
            p5.setPersonGroup(PersonGroup.SYS_ADMIN);
            personRepository.save(p5);
        }
        catch (DataAccessException ex) {
            expectedException = ex;
        }
        assertThat(expectedException, notNullValue());

        PersonCriteria personCriteria = new PersonCriteria();
        personCriteria.setNameCriteria(p1.getFamilyName());

        PagedResponse<PersonEntity> p6List = personRepository.findByCriteria(personCriteria);
        assertThat(p6List.getResults().size(), equalTo(1));
        assertThat(p6List.getResults().get(0).getPersonId(), equalTo(p1.getPersonId()));


        personRepository.delete(p1.getPersonId());
        PersonEntity p7 = personRepository.findOne(p1.getPersonId());
        assertThat(p7, nullValue());
    }
}
