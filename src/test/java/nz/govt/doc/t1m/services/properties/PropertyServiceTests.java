package nz.govt.doc.t1m.services.properties;

import nz.govt.doc.t1m.Application;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class PropertyServiceTests {

    @Value(PropertyKeys.EXAMPLE1)
    protected String example1Injected;

    @Autowired
    protected PropertyService propertyService;

    @Test
    public void testSimpleGet() {
        String value = propertyService.getPropertyValue(PropertyKeys.EXAMPLE1);
        assertThat(value, notNullValue());
    }

    @Test
    public void testInjectedValue() {
        assertThat(example1Injected, notNullValue());
    }

    @Test
    public void simpleUpdate() {
        PropertyModel propertyModel = propertyService.findProperty(PropertyKeys.EXAMPLE1);
        String valueBefore = propertyModel.getValue();
        assertThat(valueBefore, notNullValue());


        String expectedValueAfter = String.valueOf(System.currentTimeMillis());
        propertyModel.setValue(expectedValueAfter);
        propertyService.saveProperty(propertyModel);

        String valueAfter = propertyService.getPropertyValue(PropertyKeys.EXAMPLE1);
        assertThat(expectedValueAfter, equalTo(valueAfter));
    }

    @Test
    public void getAllPropertyValues() {
        PropertyCriteria criteria = new PropertyCriteria();
        PagedResponse<PropertyModel> response1 = propertyService.findByCriteria(criteria);
        assertThat(response1.getResults().size(), greaterThan(1));

        criteria.setPropertyName(PropertyKeys.EXAMPLE1);
        PagedResponse<PropertyModel> response2 = propertyService.findByCriteria(criteria);
        assertThat(response2.getResults().size(), equalTo(1));
    }

    @Test
    public void whenVisibleIsFalse() {

        // check that we can see get the property value
        String value = propertyService.getPropertyValue(PropertyKeys.INVISIBLE);
        assertThat(value, notNullValue());

        // but can't get the value when we do a findByCriteria
        PropertyCriteria criteria = new PropertyCriteria();
        criteria.setPropertyName(PropertyKeys.INVISIBLE);
        PagedResponse<PropertyModel> response = propertyService.findByCriteria(criteria);

        assertThat(response.getTotal(), equalTo(0));
        assertThat(response.getResults().size(), equalTo(0));
    }

    @Test
    public void whenIsAFileOnlyProperty() {
        // This property is defined in a property file (or perhaps some other Spring compatible property mechanism) but
        // is not stored in the database. This implies it can't be editable - but it could be visible.

        String value = propertyService.getPropertyValue(PropertyKeys.FILE_ONLY_PROPERTY);
        assertThat(value, notNullValue());

        String valueFromDB = propertyService.getPropertyValueFromDB(PropertyMetaData.FILE_ONLY_PROPERTY.getPropertyName());
        assertThat(valueFromDB, nullValue());
    }
}
