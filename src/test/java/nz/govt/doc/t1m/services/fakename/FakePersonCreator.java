package nz.govt.doc.t1m.services.fakename;

import com.bitbucket.thinbus.srp6.js.HexHashedXRoutine;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6CryptoParams;
import com.nimbusds.srp6.SRP6VerifierGenerator;
import nz.govt.doc.t1m.Application;
import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import nz.govt.doc.t1m.domain.person.PersonGroup;
import nz.govt.doc.t1m.services.credentials.AuthenticationControllerTest;
import nz.govt.doc.t1m.services.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.math.BigInteger;

/**
 */
@Component
public class FakePersonCreator {

    // Test class not scanned for ApplicationContext
    protected FakeNameGenerator fakeNameGenerator = new FakeNameGenerator();

    @Autowired
    protected PersonService personService;

    private void createPersons(int personCount) {
        FakeNameRecord[] fakeNameRecords = fakeNameGenerator.generateRecords(personCount);
        for(int i = 0; i < fakeNameRecords.length; i++) {
            FakeNameRecord nextRecord = fakeNameRecords[i];
            PersonEntity person = createOnePerson(nextRecord);
            PersonCredentials personCredentials = createPersonCredentials(person);
            personService.savePerson(person, personCredentials);
            System.out.println("Created: " + nextRecord.getEmailAddress());
        }

        System.out.println("Created persons - " + personCount);
    }

    private PersonEntity createOnePerson(FakeNameRecord nextRecord) {
        PersonEntity person = new PersonEntity();
        person.setFirstName(nextRecord.getGivenName());
        person.setFamilyName(nextRecord.getSurname());
        person.setUsername(nextRecord.getGivenName() + "." + nextRecord.getSurname());
        person.setPersonGroup(PersonGroup.SYS_ADMIN);
        return person;
    }

    private PersonCredentials createPersonCredentials(PersonEntity personEntity) {
        String username = personEntity.getUsername();
        String password = "Password01";

        SRP6CryptoParams cryptoParams = AuthenticationControllerTest.getSrp6CryptoParams();

        // Create verifier generator
        SRP6VerifierGenerator gen = new SRP6VerifierGenerator(cryptoParams);
        gen.setXRoutine(new HexHashedXRoutine());

        // Generate new salt and verifier
        BigInteger salt = new BigInteger(SRP6VerifierGenerator.generateRandomSalt(32));
        BigInteger verifier = gen.generateVerifier(salt, username, password);

        String saltHex = BigIntegerUtils.toHex(salt);
        String verifierHex = BigIntegerUtils.toHex(verifier);

        PersonCredentials personCredentials = new PersonCredentials();
        personCredentials.setPersonId(personEntity.getPersonId());
        personCredentials.setSalt(saltHex);
        personCredentials.setVerifier(verifierHex);
        return personCredentials;
    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Application.class, args);
        FakePersonCreator fakePersonCreator = context.getBean(FakePersonCreator.class);
        fakePersonCreator.createPersons(100);

    }
}
