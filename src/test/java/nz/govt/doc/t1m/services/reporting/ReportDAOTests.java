package nz.govt.doc.t1m.services.reporting;

import nz.govt.doc.t1m.Application;
import nz.govt.doc.t1m.services.reporting.domain.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ReportDAOTests {

    @Autowired ReportService reportService;

    @Autowired
    protected ReportDAO reportDAO;

    @Test
    public void testSimpleExecute() throws Exception {
        List<Report> reportList = reportService.loadReportDefinitions();
        Report report = reportList.get(0);

        ReportCriteria reportCriteria = new ReportCriteria();
        DataTable dataTable = reportDAO.executeReport(report, reportCriteria);
        System.out.println(dataTable.getResults());
    }
}
