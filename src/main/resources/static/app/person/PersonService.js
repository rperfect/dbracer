'use strict';

angular.module('myApp.PersonService',['ngResource'])

.factory('PersonSvc', ['$resource', function($resource){
    return $resource('/rest/person/:personId', {}, {
      query: {method:'GET', params:{personId:'search'}, isArray:false}
    });
}])

.factory('PersonEditSvc', ['$http', function($http) {
    var service = {

        load: function(personId, callback) {
            $http.get('/rest/person/' + personId).success(function(response) {
                callback && callback(response);
            });
        },

        save: function(person, personCredentials, callback) {
            var savePersonRequest = {
                personEntity: person,
                personCredentials: personCredentials
            };
            
            $http.post('/rest/person/', savePersonRequest ).success(function(response) {
                callback && callback(response);
            });
        }
    };

    return service;
}])

.factory('PersonCredentialsSvc', ['$http', function($http) {
    var service = {

        generateVerifier: function(username, password) {
            // client constructor requires a variable SRP6CryptoParams is defined which sets N, g, k
            var client = new SRP6JavascriptClientSessionSHA256();

            // random salt is created for each new password
            var salt = client.generateRandomSalt(); // consider passing server secure random to this method
            console.log("Salt: " + salt);

            // verifier to be generated at the browser during user registration and password reset only
            var verifier = client.generateVerifier(salt, username, password);
            console.log("Verifier: " + verifier);
            
            var personCredentials = {
                salt: salt,
                verifier: verifier
            };
            
            return personCredentials;
        }
    };

    return service;
}])
    
.factory('PersonSearchSvc', ['$http', function($http) {
    var service = {
        searchCriteria: {
            pageNumber : 1,
            pageSize : 6,
            nameCriteria : null
        },

        search: function(callback) {
            $http.post('/rest/person/search', service.searchCriteria).success(function(response) {
                service.searchResponse = response;
                //service.numPages = Math.floor(response.total / response.pageSize) + 1;
                callback && callback(response);
            });
        },

        reset: function() {
            // For various UI databinding reasons we can just blow away service.searchCriteria because that
            // causes problems, so we need to reset the fields separately.
            service.searchCriteria.nameCriteria = null;
            service.searchResponse = {};
            //service.numPages = 0;
        }
    };

    service.reset();

    return service;
}]);
