'use strict';

angular.module('myApp.PersonEditView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/person/edit/:personId', {
    templateUrl: '/app/person/PersonEditView.html',
    controller: 'PersonEditCtrl'
  });
}])

.controller('PersonEditCtrl', ['$scope', '$routeParams', '$window', 'PersonEditSvc', 'PersonCredentialsSvc', function($scope, $routeParams, $window, PersonEditSvc, PersonCredentialsSvc) {

    // TODO: replace with proper security
    $scope.currentUser = {};
    $scope.currentUser.administrator = true;

    $scope.editMode = false;


    $scope.refresh = function() {
        var personId = $routeParams.personId;
        if(personId > 0) {
            PersonEditSvc.load(personId, function (response) {
                $scope.selectedPerson = response;
            });
        }
        else {
            $scope.selectedPerson = {};
            $scope.editMode = true;
        }
    };

    $scope.editAction = function() {
        console.log("editAction() - clicked");
        $scope.editMode = true;
    };

    $scope.cancelAction = function() {
        console.log("cancelAction() - clicked");
        $scope.editMode = false;

        if($routeParams.personId > 0) {
            // if not new reload the selected report to get rid of any edits
            $scope.refresh();
        }
        else {
            $window.location.href = '#/person/search';
        }
    };

    $scope.saveAction = function() {
        var selectedPerson = $scope.selectedPerson.model;
        var personCredentials = null;
        if(selectedPerson.passwordEdit != null && selectedPerson.passwordEdit === selectedPerson.passwordConfirm) {
            personCredentials = PersonCredentialsSvc.generateVerifier(selectedPerson.username, selectedPerson.passwordEdit);
        }
        
        PersonEditSvc.save($scope.selectedPerson.model, personCredentials, function (response) {
            console.log("responseMessages = " + response.responseMessages);
            var hasNoErrors = response.responseMessages.length === 0;
            if(hasNoErrors) {
                // redirect the browser with the saved entities entityId so that brand new entities get switched to the right url
                $window.location.href = '#/person/edit/' + response.model.personId;
                $scope.editMode = false;
                $scope.refresh();
            }
        });
    };

    $scope.refresh();
}]);
