'use strict';

angular.module('myApp.PersonSearchView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/person/search', {
        templateUrl: '/app/person/PersonSearchView.html',
        controller: 'PersonSearchCtrl'
    });
}])

.controller('PersonSearchCtrl', ['$scope', '$window', 'PersonSearchSvc', function($scope, $window, PersonSearchSvc) {

    $scope.personSearchSvc = PersonSearchSvc;

    $scope.pageChanged = function() {
        PersonSearchSvc.search();
    };

    $scope.searchAction = function() {
        // user clicking the search button always resets the pageNumber
        PersonSearchSvc.searchCriteria.pageNumber = 1;
        $scope.pageChanged();
    }

    $scope.resetAction = function() {
        PersonSearchSvc.reset();
        $scope.searchAction();
    }

    $scope.newAction = function() {
        $window.location.href = '#/person/edit/-1';
    }

    $scope.pageChanged();
}]);
