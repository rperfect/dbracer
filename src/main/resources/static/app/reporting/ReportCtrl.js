'use strict';

angular.module('myApp.ReportView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/report/view/:reportName', {
        templateUrl: '/app/reporting/ReportView.html',
        controller: 'ReportCtrl'
    });
}])

.controller('ReportCtrl', ['$scope', '$window', '$routeParams', 'ReportSvc', 'ReportExecuteSvc', function($scope, $window, $routeParams, ReportSvc, ReportExecuteSvc) {

    $scope.reportSvc = ReportSvc;
    $scope.reportExecuteSvc = ReportExecuteSvc;
    $scope.reportExecuteSvc.searchCriteria.name = $routeParams.reportName;
    $scope.report = ReportSvc.get({reportName: $routeParams.reportName});

    $scope.pageChanged = function() {
        ReportExecuteSvc.search();
    };

    $scope.searchAction = function() {
        // user clicking the search button always resets the pageNumber
        ReportExecuteSvc.searchCriteria.pageNumber = 1;
        $scope.pageChanged();
    }

    $scope.resetAction = function() {
        ReportExecuteSvc.reset();
        $scope.searchAction();
    }

    $scope.pageChanged();
}]);
