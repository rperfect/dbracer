'use strict';

angular.module('myApp.ReportService',['ngResource'])

.factory('ReportSvc', ['$resource', function($resource){
    return $resource('/rest/report/:reportName', {}, {
      query: {method:'GET', params:{reportName:'list'}, isArray:true}
    });
}])

.factory('ReportExecuteSvc', ['$http', function($http) {
    var service = {
        searchCriteria: {
            pageNumber : 1,
            pageSize : 10,
            parameterList :[]
        },

        search: function(callback) {
            $http.post('/rest/report/execute', service.searchCriteria).success(function(response) {
                service.searchResponse = response;
                if(callback) {
                    callback(response);
                }
            });
        },

        reset: function() {
            // For various UI databinding reasons we can just blow away service.searchCriteria because that
            // causes problems, so we need to reset the fields separately.
            service.searchCriteria.parameterList = [];
            service.searchResponse = {};
        }
    };

    service.reset();

    return service;
}]);
