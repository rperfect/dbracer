'use strict';

angular.module('myApp.ReportListView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/reportList', {
        templateUrl: '/app/report/ReportListView.html',
        controller: 'ReportListCtrl'
    });
}])

.controller('ReportListCtrl', ['$scope', '$window', 'ReportSvc', function($scope, $window, ReportSvc) {

    $scope.reportNameList = ReportSvc.query();


}]);
