'use strict';

angular.module('myApp.ExceptionService',['ngResource'])

.factory('ExceptionSvc', [ function() {
    var service = {
        exceptionMessage: null
    };

    return service;
}]);
