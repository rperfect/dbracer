'use strict';

angular.module('myApp.ExceptionView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/exception', {
        templateUrl: '/app/exception/ExceptionView.html',
        controller: 'ExceptionCtrl'
    });
}])

.controller('ExceptionCtrl', ['$scope', '$window', 'ExceptionSvc', function($scope, $window, ExceptionSvc) {

    $scope.exceptionMessage = ExceptionSvc.exceptionMessage;

}]);
