'use strict';

// IE workaround, create fake console if one doesn't already exist
window.console = window.console || {};
window.console.log = window.console.log || function() {};


// Declare app level module which depends on filters, and services
angular.module('myApp', [
    'ngRoute',
    'ui.bootstrap',
    'uuid4',
    
    'myApp.AuditEventSearchView',
    'myApp.AuditEventService',

    'myApp.ExceptionView',
    'myApp.ExceptionService',

    'myApp.LandingView',
    'myApp.LandingService',

	'myApp.LoginView',
    'myApp.LoginService',
    'myApp.MenuView',

    'myApp.PersonSearchView',
    'myApp.PersonEditView',
    'myApp.PersonService',

    'myApp.PropertySearchView',
    'myApp.PropertyEditView',
    'myApp.PropertyService',

    'myApp.ReportView',
    'myApp.ReportService',

    'myApp.ReportListView'
])

.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {

    /*$routeProvider.otherwise({
        redirectTo: '/person/search'
    });*/

    //http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer
    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
      	$httpProvider.defaults.headers.get = {};
    }

    // disable IE ajax request caching
    //$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

    // spring-security-angular (dsyer)
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}])

.controller('navigation', function($scope, $route) {

	$scope.tab = function(route) {
		return $route.current && route === $route.current.controller;
	};
});




