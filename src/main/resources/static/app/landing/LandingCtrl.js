'use strict';

angular.module('myApp.LandingView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/landing', {
        templateUrl: '/app/landing/LandingView.html',
        controller: 'LandingCtrl'
    });
}])


.controller('LandingCtrl', ['$scope', 'LandingSvc', function($scope, LandingSvc) {
    $scope.publicAccessURL = LandingSvc.publicAccessURL();
    $scope.secureLoginURL  = LandingSvc.secureLoginURL();
}]);
