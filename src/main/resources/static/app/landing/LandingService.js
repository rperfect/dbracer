'use strict';

angular.module('myApp.LandingService',[])


.factory('LandingSvc', ['$location', function($location) {

    var service = {

        getTLSPort: function() {
            var tlsPort = '';
            if($location.port() == 6080 || $location.port() == 6443) {
                tlsPort = ':6443';
            }

            // HACK for now
            tlsPort = ':6080';

            return tlsPort;
        },

        publicAccessURL: function() {
            //return 'https://' + $location.host() + service.getTLSPort() + '/app/main';
            return 'http://' + $location.host() + service.getTLSPort() + '/app/main';
        },

        secureLoginURL:  function() {
            //return 'https://' + $location.host() + service.getTLSPort() + '/secureLogin';
            return 'http://' + $location.host() + service.getTLSPort() + '/secureLogin';
        }

    };

    return service;
}]);
