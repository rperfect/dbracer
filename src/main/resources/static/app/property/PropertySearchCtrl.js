'use strict';

angular.module('myApp.PropertySearchView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/property/search', {
    templateUrl: '/app/property/PropertySearchView.html',
    controller: 'PropertySearchCtrl'
  });
}])


.controller('PropertySearchCtrl', ['$scope', '$window', 'PropertySearchSvc', function($scope, $window, PropertySearchSvc) {

    $scope.propertySearchSvc = PropertySearchSvc;

    $scope.pageChanged = function() {
        PropertySearchSvc.search();
    };

    $scope.searchAction = function() {
        // user clicking the search button always resets the pageNumber
        PropertySearchSvc.searchCriteria.pageNumber = 1;
        $scope.pageChanged();
    }

    $scope.resetAction = function() {
        PropertySearchSvc.reset();
        $scope.searchAction();
    }

    $scope.pageChanged();
}]);
