'use strict';

angular.module('myApp.PropertyService',[])

.factory('PropertySearchSvc', ['$http', function($http) {
    var service = {
        searchCriteria: {
            pageNumber : 1,
            pageSize : 6,
            propertyName : null
        },

        search: function(callback) {
            $http.post('/rest/property/search', service.searchCriteria).success(function(response) {
                service.searchResponse = response;
                if(callback) {
                    callback(response);
                }
            });
        },

        reset: function() {
            // For various UI databinding reasons we can just blow away service.searchCriteria because that
            // causes problems, so we need to reset the fields separately.
            service.searchCriteria.nameCriteria = null;
            service.searchResponse = {};
        }
    };

    service.reset();

    return service;
}])

.factory('PropertySvc', ['$resource', function($resource){
    return $resource('/rest/property/:propertyName', {}, {});
}]);
