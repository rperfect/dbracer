'use strict';

angular.module('myApp.PropertyEditView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/property/edit/:propertyName', {
    templateUrl: '/app/property/PropertyEditView.html',
    controller: 'PropertyEditCtrl'
  });
}])

.controller('PropertyEditCtrl', ['$scope', '$routeParams', '$window', 'PropertySvc', function($scope, $routeParams, $window, PropertySvc) {

    // TODO: replace with proper security
    $scope.currentUser = {};
    $scope.currentUser.administrator = true;

    $scope.editMode = false;


    $scope.refresh = function() {
        var propertyName = $routeParams.propertyName;
        if(propertyName !== null) {
            $scope.selectedProperty = PropertySvc.get({propertyName: propertyName});
        }
    }

    $scope.editAction = function() {
        $scope.editMode = true;
    }

    $scope.cancelAction = function() {
        $scope.editMode = false;

        if($routeParams.propertyName !== null) {
            // if not new reload the selected report to get rid of any edits
            $scope.refresh();
        }
        else {
            $window.location.href = '#/property/search';
        }
    }

    $scope.saveAction = function() {
        $scope.selectedProperty.$save(function(data) {
            var hasNoErrors = data.responseMessages.length === 0;
            if(hasNoErrors) {
                // redirect the browser with the saved model/entity's "id" so that the browser gets switched to the right url
                $window.location.href = '#/property/edit/' + data.model.name;
                $scope.editMode = false;
                $scope.refresh();
            }
        });
    }

    $scope.resetAction = function() {
        $scope.selectedProperty.model.value = $scope.selectedProperty.model.defaultValue;
    }

    $scope.refresh();
}]);
