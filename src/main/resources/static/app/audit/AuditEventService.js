'use strict';

angular.module('myApp.AuditEventService',['ngResource'])

    .factory('AuditEventSvc', ['$resource', function($resource){
        return $resource('/rest/audit/:personId', {}, {
            query: {method:'GET', params:{personId:'search'}, isArray:false}
        });
    }])


    .factory('AuditEventSearchSvc', ['$http', function($http) {
        var service = {
            searchCriteria: {
                pageNumber : 1,
                pageSize : 6,
                requestId : null
            },

            performMeasurements: function (measurementCriteria, callback) {
                $http.post('/rest/audit/performMeasurements', measurementCriteria).success(function(response) {
                    callback && callback(response);
                });
            },

            getStatus: function (callback) {
                $http.get('/rest/audit/status').success(function(response) {
                    callback && callback(response);
                });
            },

            search: function(callback) {
                $http.post('/rest/audit/search', service.searchCriteria).success(function(response) {
                    service.searchResponse = response;
                    //service.numPages = Math.floor(response.total / response.pageSize) + 1;
                    callback && callback(response);
                });
            },

            reset: function() {
                // For various UI databinding reasons we can just blow away service.searchCriteria because that
                // causes problems, so we need to reset the fields separately.
                service.searchCriteria.requestId = null;
                service.searchResponse = {};
                //service.numPages = 0;
            }
        };

        service.reset();

        return service;
    }]);
