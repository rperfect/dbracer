'use strict';

angular.module('myApp.AuditEventSearchView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/audit/search', {
        templateUrl: '/app/audit/AuditEventSearchView.html',
        controller: 'AuditEventSearchCtrl'
    });
}])

.controller('AuditEventSearchCtrl', ['$scope', '$window', '$interval', 'AuditEventSearchSvc', function($scope, $window, $interval, AuditEventSearchSvc) {

    $scope.auditEventSearchSvc = AuditEventSearchSvc;
    $scope.measurementCriteria = {
        name: 'Default',
        iterationSize: 100,
        iterationCount: 5,
        sampleSize: 10,
        storageType: "SQLServer",  // MySQL, Auroa, SQLServer, DynamoDB
        resetTable: false
    };

    $scope.status = 'Idle';
    $scope.intervalPromise;

    $scope.onPerformMeasurementsAction = function () {
        $scope.status = "Started";
        $scope.startPolling();
        console.log('performMeasurements - invoked');
        AuditEventSearchSvc.performMeasurements($scope.measurementCriteria, function (response) {
            console.log('performMeasurements - started...');
        });
    };

    $scope.onRefreshStatus = function () {
        AuditEventSearchSvc.getStatus(function (response) {
            $scope.status = response;
            if($scope.status === 'Finished') {
                $scope.stopPolling();
            }
        });
    };

    $scope.startPolling = function() {
        $scope.intervalPromise = $interval(function(){ $scope.onRefreshStatus(); }, 3000);
    };

    $scope.stopPolling = function() {
        if($scope.intervalPromise) {
            $interval.cancel($scope.intervalPromise);
        }
    };

}]);
