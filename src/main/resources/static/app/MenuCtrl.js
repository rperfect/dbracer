'use strict';

angular.module('myApp.MenuView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
}])

.controller('MenuCtrl', ['$scope', '$location', 'LoginSvc', function($scope, $location, LoginSvc) {

    $scope.checkAuthenticated = function() {
        return LoginSvc.isAuthenticated();
    };

    $scope.isActive = function(viewLocation) {
        return $location.path().startsWith(viewLocation);
    };
}]);
