'use strict';

angular.module('myApp.LoginView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: '/app/login/LoginView.html',
        controller: 'LoginCtrl'
    });
}])

.controller('LoginCtrl', ['$scope', '$window', '$http', '$location', 'uuid4', 'LoginSvc', function($scope, $window, $http, $location, uuid4, LoginSvc) {

    $scope.credentials = {};

    // Called once on application load and for page refresh
    if(LoginSvc.isAuthenticated()) {
        if($location.path().length == 0) {
            $window.location.href = "/#/person/search";
        }
        // else let the browser use the current URL
    }
    else {
        $window.location.href = "/#/login";
    }

    $scope.checkAuthenticated = function() {
        return LoginSvc.isAuthenticated();
    };


    $scope.loginAction = function () {
        console.log("SRPLogin");

        // hardcoding credentials for now...
        var username = $scope.credentials.username;
        var password = $scope.credentials.password;
        var requestId = uuid4.generate();

            // client constructor requires a variable SRP6CryptoParams is defined which sets N, g, k
        var client = new SRP6JavascriptClientSessionSHA256();

        // normal login flow step1a client: browser starts with username and password given by user at the browser
        client.step1(username, password);

        // send username to server to get salt and B back
        var step1Request = {
            requestId: requestId,
            username: username
        };

        LoginSvc.authenticateStep1(step1Request, function (step1Result) {

            // normal login flow step2a client: server sends users salt from user registration and the server ephemeral number
            var credentials = client.step2(step1Result.salt, step1Result.b);

            var step2Request = {
                requestId: requestId,
                a: credentials.A,
                m1: credentials.M1
            };

            // make server call to get m2 from (a,m1)
            LoginSvc.authenticateStep2(step2Request, function (step2Result) {
                var m2 = step2Result.m2;

                // normal login flow step3 client: client verifies that the server shows proof of the shared session key which demonstrates that it knows actual verifier
                client.step3(m2);

                // the javascript client defaults to hashing the session key as that is additional protection of the password in case the key is accidentally exposed to an attacker.
                if(client.getSessionKey() == client.getSessionKey(true)) {
                    console.log("Authentication successful. Session = " + client.getSessionKey());
                }
                else {
                    throw "Invalid session";
                }

                if (LoginSvc.isAuthenticated()) {
                    $window.location.href = "/#/person/search";
                    $scope.error = false;
                } else {
                    $window.location.href = "/#/login";
                    $scope.error = true;
                }
            })
        });
    }

    $scope.logout = function() {
        LoginSvc.logout();
        $scope.error = false;
        $window.location.href = "/#/login";
    }


}]);
