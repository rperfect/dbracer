'use strict';

angular.module('myApp.LoginService',[])

.factory('sessionInjector', ['$window', function($window) {
    var sessionInjector = {
        request: function(config) {
            if ($window.localStorage.jwtToken) {
                config.headers['Authorization'] = $window.localStorage.jwtToken;
            }
            return config;
        }
    };
    return sessionInjector;
}])

.factory('authorizationFailure', ['$window', function($window) {
    var authorizationFailure = {
        responseError: function(response) {
            if (response.status == 401){
                $window.location.href = "/#/login";
            }
        }
    };
    return authorizationFailure;
}])

.factory('exceptionResponse', ['$window', 'ExceptionSvc', function($window, ExceptionSvc) {
    var exceptionResponse = {
        responseError: function(response) {
            if (response.status == 500){
                ExceptionSvc.exceptionMessage = response.data;
                $window.location.href = "/#/exception";
            }
        }
    };
    return exceptionResponse;
}])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('sessionInjector');
    $httpProvider.interceptors.push('authorizationFailure');
    $httpProvider.interceptors.push('exceptionResponse');
}])

.factory('LoginSvc', ['$window', '$http', function($window, $http) {

    var service = {

        user: null,

        isAuthenticated: function() {
            return $window.localStorage.jwtToken && $window.localStorage.jwtToken.length > 0;
        },

        authenticateStep1: function(step1Data, callback) {
            $http.post('/authenticate/step1', step1Data).success(function(data) {
                callback && callback(data);
            }).error(function() {
                callback && callback();
            });
        },

        authenticateStep2: function(step1Data, callback) {
            $http.post('/authenticate/step2', step1Data).success(function(response) {
                if (response.jwtToken) {
                    $window.localStorage.jwtToken = response.jwtToken;
                }
                else {
                    $window.localStorage.removeItem('jwtToken');
                }

                callback && callback(response);
            }).error(function() {
                $window.localStorage.removeItem('jwtToken');
                callback && callback();
            });
        },

        getUser: function() {
            return service.user;
        },

        logout:  function() {
            $window.localStorage.removeItem('jwtToken');
        }

    };

    return service;
}]);
