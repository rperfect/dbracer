package nz.govt.doc.t1m;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class CurrentDateController {
 
	private final AtomicLong counter = new AtomicLong();
 
	@RequestMapping("/currentdate/{pattern}")
	public FormattedDate formatCurrentDate(@PathVariable final String pattern) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern(pattern);
 
		return new FormattedDate(counter.incrementAndGet(), pattern, fmt.format(LocalDateTime.now()));
	}
}