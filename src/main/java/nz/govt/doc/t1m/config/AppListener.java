package nz.govt.doc.t1m.config;

import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import nz.govt.doc.t1m.domain.person.PersonGroup;
import nz.govt.doc.t1m.domain.response.Response;
import nz.govt.doc.t1m.services.credentials.PersonCredentialsService;
import nz.govt.doc.t1m.services.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class AppListener implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    protected PersonService personService;

    @Autowired
    protected PersonCredentialsService personCredentialsService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        System.out.println("Application is READY!");

        Response<PersonEntity> response = personService.findPersonByUsername("Admin");
        if(response.getModel() == null) {
            PersonEntity personEntity = new PersonEntity();
            personEntity.setUsername("Admin");
            personEntity.setFirstName("Administrator");
            personEntity.setPersonGroup(PersonGroup.SYS_ADMIN);

            PersonCredentials personCredentials = personCredentialsService.createPersonCredentials(personEntity.getUsername(), "Password01");
            personService.savePerson(personEntity, personCredentials);
        }
    }
}
