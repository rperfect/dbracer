package nz.govt.doc.t1m;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.service.ApiKey;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.security.Principal;
import java.security.SecureRandom;


@Configuration
@ComponentScan("nz.govt.doc.t1m")
//@EnableSwagger2
//@EnableAspectJAutoProxy
@EnableAutoConfiguration
@EnableAsync
@EnableTransactionManagement
@RestController
public class Application {

    //@Autowired
    //private TypeResolver typeResolver;

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

    @Bean
    public PasswordEncoder bcryptPasswordEncoder() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12, new SecureRandom());
        return passwordEncoder;
    }


    private ApiKey apiKey() {
        return new ApiKey("mykey", "api_key", "header");
    }

    /*@Bean
    public Docket docket() {
        ResponseMessage error = new ResponseMessageBuilder().code(500).message("500 message").responseModel(new ModelRef("Error")).build();
        ArrayList<ResponseMessage> responseMessages = new ArrayList<ResponseMessage>();
        responseMessages.add(error);

        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build().pathMapping("/")
                .directModelSubstitute(LocalDate.class, String.class).genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(typeResolver.resolve(DeferredResult.class, typeResolver.resolve(ResponseEntity.class, WildcardType.class)), typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, responseMessages );
                //.securitySchemes(new ArrayList(apiKey())).securityContexts(new ArrayList(securityContext()));
    }*/


    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
    }
}
