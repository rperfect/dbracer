package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.criteria.AbstractCriteria;
import nz.govt.doc.t1m.domain.criteria.PagedCriteria;

import java.util.Date;

/**
 */
public class AuditEventCriteria extends AbstractCriteria {


    private String userId;
    private String eventType;

    private Date fromEventTime;
    private Date toEventTime;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Date getFromEventTime() {
        return fromEventTime;
    }

    public void setFromEventTime(Date fromEventTime) {
        this.fromEventTime = fromEventTime;
    }

    public Date getToEventTime() {
        return toEventTime;
    }

    public void setToEventTime(Date toEventTime) {
        this.toEventTime = toEventTime;
    }
}
