package nz.govt.doc.t1m.services.audit.jpa;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.services.audit.AuditEventDAO;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 */
public interface AuditEventRepository extends PagingAndSortingRepository<AuditEvent, String>, AuditEventRepositoryCustom {

}
