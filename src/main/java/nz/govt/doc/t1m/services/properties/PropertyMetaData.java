package nz.govt.doc.t1m.services.properties;


/**
 */
enum PropertyMetaData {

    EXAMPLE1(MetaDataFactoryImpl.create(PropertyKeys.EXAMPLE1, "e1", "Example 1 property description").visible().editable()),
    INVISIBLE(MetaDataFactoryImpl.create(PropertyKeys.INVISIBLE, "e2", "Invisible property - should not be seen in GUI")),
    FILE_ONLY_PROPERTY(MetaDataFactoryImpl.create(PropertyKeys.FILE_ONLY_PROPERTY, null, "File only property - not in DB, not seen in GUI")),

    DATASOURCE_URL(MetaDataFactoryImpl.create("spring.datasource.url", null, "Data source JDBC URL").visible().editable()),
    DATASOURCE_USERNAME(MetaDataFactoryImpl.create("spring.datasource.username", null, "Data source username.").visible().editable()),
    DATASOURCE_PASSWORD(MetaDataFactoryImpl.create("spring.datasource.password", null, "Data source password.").visible().editable().hidden()),
    DATASOURCE_DRIVER_CLASS_NAME(MetaDataFactoryImpl.create("spring.datasource.driver-class-name", null, "Data source driver class").visible());

    private String propertyName;
    private String defaultValue;
    private String description;
    private boolean visible;
    private boolean editable;
    private boolean secret;

    PropertyMetaData(MetaDataFactory factory) {
        MetaDataFactoryImpl pkFactoryImpl = (MetaDataFactoryImpl)factory;
        this.propertyName = pkFactoryImpl.getName();
        this.defaultValue = pkFactoryImpl.getDefaultValue();
        this.description = pkFactoryImpl.getDescription();
        this.visible = pkFactoryImpl.isVisible();
        this.editable = pkFactoryImpl.isEditable();
        this.secret = pkFactoryImpl.isSecret();
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isSecret() {
        return secret;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
