package nz.govt.doc.t1m.services.audit;


/**
 */
public interface MeasurementAction {

    public void performMeasurement(MeasurementContext measurementContext);
}
