package nz.govt.doc.t1m.services.reporting.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class View {

    @XmlElement(name = "column")
    @XmlElementWrapper(name = "columnList")
    private List<Column> columnList;

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }
}
