package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.audit.Measurement;
import nz.govt.doc.t1m.services.audit.dynamodb.AuditEventDynoDAO;
import nz.govt.doc.t1m.services.audit.jpa.AuditEventRepository;
import nz.govt.doc.t1m.services.audit.jpa.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class AuditEventService {

    @Autowired
    protected AuditEventRepository auditEventRepository;

    @Autowired
    protected AuditEventDynoDAO auditEventDynoDAO;

    @Autowired
    protected AuditEventFactory auditEventFactory;

    @Autowired
    protected MeasurementAction[] measurementActions;

    @Autowired
    protected MeasurementRepository measurementRepository;

    // active context (if running)
    private MeasurementContext measurementContext;

    public void saveAuditEvent(AuditEvent auditEvent) {
        //auditEventRepository.saveAuditEvent(auditEvent);
    }

    public void createLotsOfAuditEvents(MeasurementContext measurementCtx) {
        System.out.println("Started: createLotsOfAuditEvents()");
        measurementCtx.startTimer();

        for( int i  = 0; i < measurementCtx.getIterationSize(); i++) {
            AuditEvent auditEvent = auditEventFactory.createNext();
            measurementCtx.getAuditEventDAO().saveAuditEvent(auditEvent);

            if(measurementCtx.getSampleList().size() < measurementCtx.getSampleSize() && Math.random() < 0.50) {
                measurementCtx.getSampleList().add(auditEvent);
            }

            int percentComplete = Math.round((float)i / measurementCtx.getIterationSize() * 100);
            measurementCtx.setStatus(measurementCtx.getMeasurement().getName() + ": Create-" + percentComplete + "%");
        }

        long totalDuration = measurementCtx.endTimerAndGetTotalDuration();
        float createOneEventDuration = (float) totalDuration / measurementCtx.getIterationSize();

        measurementCtx.getMeasurement().setCreateAllEvents(totalDuration);
        measurementCtx.getMeasurement().setCreateOneEvent(createOneEventDuration);
        System.out.println("Finished: createLotsOfAuditEvents()");
    }


    @Async
    public void performMeasurements(MeasurementCriteria measurementCriteria) {
        // switch between the different implementation strategies
        AuditEventDAO auditEventDAO = auditEventRepository;
        if(measurementCriteria.getStorageType() != null && measurementCriteria.getStorageType().equals("DynamoDB")) {
            auditEventDAO = auditEventDynoDAO;
        }

        if(measurementCriteria.getName() == null) {
            measurementCriteria.setName("Default");
        }

        MeasurementContext measurementCtx = new MeasurementContext(measurementCriteria, auditEventDAO, auditEventFactory);
        measurementContext = measurementCtx;

        for(int i = 0; i < measurementCtx.getIterationCount(); i++) {
            measurementCtx.createNextMeasurement(i);
            performOneIteration(measurementCtx);
        }

        measurementCtx.setStatus("Finished");
    }

    private void performOneIteration(MeasurementContext measurementCtx) {
        createLotsOfAuditEvents(measurementCtx);

        for(MeasurementAction nextAction : measurementActions) {
            System.out.println("Started: " + nextAction.getClass().getSimpleName());
            nextAction.performMeasurement(measurementCtx);
        }

        Measurement measurement = measurementCtx.getMeasurement();
        measurementRepository.save(measurement);
    }


    public String getMeasurementStatus() {
        return measurementContext != null ? measurementContext.getStatus() : "Idle";
    }
}
