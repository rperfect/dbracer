package nz.govt.doc.t1m.services.properties;

/**
 */
public interface PropertyKeys {

    String EXAMPLE1 = "example1";
    String INVISIBLE = "example.invisible";
    String FILE_ONLY_PROPERTY = "example.fileOnlyProperty";
}
