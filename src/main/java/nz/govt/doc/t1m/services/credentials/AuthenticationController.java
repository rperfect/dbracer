package nz.govt.doc.t1m.services.credentials;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authenticate")
public class AuthenticationController {

    //@Autowired
    //protected AuthenticationManager authenticationManager;

    //@Autowired
    //protected UserDetailsService userDetailsService;

    //@Autowired
    //protected JwtTokenService tokenService;

    @Autowired
    protected ThinbusSRPService thinbusSRPService;

    /*@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> authenticationRequest(@RequestBody AuthenticationRequest authenticationRequest) throws AuthenticationException {

        final String username = authenticationRequest.getUsername();
        final String password = authenticationRequest.getPassword();

        // Perform the authentication
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload post-authentication so we can generate token
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String token = this.tokenService.generateToken(userDetails);

        // Return the token
        return ResponseEntity.ok(new AuthenticationResponse(token));
    }*/

    @RequestMapping(value = "/step1", method = RequestMethod.POST)
    public ResponseEntity<Step1Data> authenticateStep1(@RequestBody Step1Data step1Data) throws AuthenticationException {
        Step1Data step1DataResult = thinbusSRPService.serverStep1(step1Data);
        return ResponseEntity.ok(step1DataResult);
    }

    @RequestMapping(value = "/step2", method = RequestMethod.POST)
    public ResponseEntity<Step2Data> authenticateStep2(@RequestBody Step2Data step2Data) throws AuthenticationException {
        Step2Data step2DataResult = thinbusSRPService.serverStep2(step2Data);
        return ResponseEntity.ok(step2DataResult);
    }
}
