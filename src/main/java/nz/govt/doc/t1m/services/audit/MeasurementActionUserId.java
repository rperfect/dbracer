package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 */
@Component
public class MeasurementActionUserId implements MeasurementAction {

    @Override
    public void performMeasurement(MeasurementContext measurementCtx) {
        measurementCtx.startTimer();

        for(int i = 0; i < measurementCtx.getSampleSize(); i++) {
            int randomIdx = new Random().nextInt(measurementCtx.getSampleList().size());
            AuditEvent auditEvent = measurementCtx.getSampleList().get(randomIdx);

            AuditEventCriteria criteria = new AuditEventCriteria();
            criteria.setPageSize(50);
            criteria.setUserId(auditEvent.getUserId());

            PagedResponse<AuditEvent> pagedResponse = measurementCtx.getAuditEventDAO().findByCriteria(criteria);
            System.out.print(pagedResponse.getResults().size() + ", ");

            int percentComplete = Math.round((float)i / measurementCtx.getSampleSize() * 100);
            measurementCtx.setStatus(measurementCtx.getMeasurement().getName() + ": " + getClass().getSimpleName() + "-" + percentComplete + "%");
        }
        System.out.println();

        measurementCtx.getMeasurement().setUserId(measurementCtx.endTimerAndGetSampleDuration());
    }
}
