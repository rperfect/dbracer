package nz.govt.doc.t1m.services.properties;

import nz.govt.doc.t1m.domain.properties.PropertyEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 */
public interface PropertyRepository extends PagingAndSortingRepository<PropertyEntity, String> {

    PropertyEntity findOneByName(String name);
}
