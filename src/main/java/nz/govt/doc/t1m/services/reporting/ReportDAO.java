package nz.govt.doc.t1m.services.reporting;

import nz.govt.doc.t1m.services.reporting.domain.Column;
import nz.govt.doc.t1m.services.reporting.domain.Query;
import nz.govt.doc.t1m.services.reporting.domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
@Repository
public class ReportDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    protected DataSource dataSource;
    private String databaseProductName;

    private String getDatabaseProductName() {
        try {
            if(databaseProductName == null) {
                Connection connection = dataSource.getConnection();
                DatabaseMetaData metaData = connection.getMetaData();
                databaseProductName = metaData.getDatabaseProductName();
            }

            return databaseProductName;
        }
        catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public DataTable executeReport(final Report report, final ReportCriteria reportCriteria) {

        long offset = reportCriteria.getOffset();
        int count = reportCriteria.getPageSize();

        // TODO: MySQL and SQL Server do it differently
        String limitSQL;
        if(getDatabaseProductName().equals("Microsoft SQL Server")) {
            //String limitSQL = " Order by p.personId OFFSET " + offset + " ROWS FETCH NEXT " + count + " ROWS ONLY";
            limitSQL = " Order by m.measurementId OFFSET " + offset + " ROWS FETCH NEXT " + count + " ROWS ONLY";
        }
        else {
            limitSQL = " LIMIT " + offset + ", " + count;
        }


        Query query = report.getQuery();
        String selectSQL = query.getSelect();
        String fromWhereSQL = query.getFromWhere();

        if(selectSQL == null || selectSQL.trim().length() == 0) {
            selectSQL = "Select * ";
        }

        String dataSQL = selectSQL + fromWhereSQL + limitSQL;
        String countSQL = "Select count(*) "  + fromWhereSQL;
        System.out.println("***  dataSQL: " + dataSQL);
        System.out.println("*** countSQL: " + countSQL);

        SqlParameterSource namedParameters = new MapSqlParameterSource();

        List<DataRow> dataRows = jdbcTemplate.query(dataSQL, namedParameters, new RowMapper<DataRow>() {
            @Override
            public DataRow mapRow(ResultSet rs, int rowNum) throws SQLException {

                DataRow dataRow = new DataRow();

                NumberFormat numberFormat = new DecimalFormat("###,###.##");

                ResultSetMetaData metaData = rs.getMetaData();
                Map<String, Integer> columnMap = getColumnMap(metaData);

                List<Column> columnList = report.getView().getColumnList();
                for (int i = 0; i < columnList.size(); i++) {
                    Column nextCol = columnList.get(i);
                    Object value = rs.getObject(nextCol.getName());

                    int colIdx = columnMap.get(nextCol.getName());
                    int columnType = metaData.getColumnType(colIdx);

                    String dataCellValue;
                    if(columnType == Types.DOUBLE) {
                        dataCellValue = numberFormat.format(value);
                    }
                    else {
                        dataCellValue = String.valueOf(value);
                    }

                    DataCell dataCell = new DataCell();
                    dataCell.setName(nextCol.getName());
                    dataCell.setValue(dataCellValue);

                    dataRow.getCells().add(dataCell);
                }

                return dataRow;
            }

            private Map<String, Integer> getColumnMap(ResultSetMetaData metaData) throws SQLException {
                Map<String, Integer> columnMap = new HashMap<>();
                for(int i = 1; i <= metaData.getColumnCount(); i++) {
                    String columnName = metaData.getColumnName(i);
                    columnMap.put(columnName, i);
                }
                return columnMap;
            }
        });

        Integer totalRowCount = jdbcTemplate.queryForObject(countSQL, namedParameters, Integer.class);

        DataTable dataTable = new DataTable();
        dataTable.setResults(dataRows);
        dataTable.setPage(reportCriteria.getPageNumber());
        dataTable.setPageSize(reportCriteria.getPageSize());
        dataTable.setTotal(totalRowCount);
        dataTable.setNumberOfPages((int)Math.floor((double)totalRowCount / reportCriteria.getPageSize()) + 1);

        return dataTable;
    }
}
