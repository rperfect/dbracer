package nz.govt.doc.t1m.services.person;

import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;

/**
 */
public class SavePersonRequest {

    private PersonEntity personEntity;
    private PersonCredentials personCredentials;


    public PersonEntity getPersonEntity() {
        return personEntity;
    }

    public void setPersonEntity(PersonEntity personEntity) {
        this.personEntity = personEntity;
    }

    public PersonCredentials getPersonCredentials() {
        return personCredentials;
    }

    public void setPersonCredentials(PersonCredentials personCredentials) {
        this.personCredentials = personCredentials;
    }
}
