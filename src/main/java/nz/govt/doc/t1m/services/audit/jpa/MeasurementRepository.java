package nz.govt.doc.t1m.services.audit.jpa;

import nz.govt.doc.t1m.domain.audit.Measurement;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 */
public interface MeasurementRepository extends PagingAndSortingRepository<Measurement, String> {
}
