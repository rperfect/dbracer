package nz.govt.doc.t1m.services.properties;

import nz.govt.doc.t1m.domain.criteria.AbstractCriteria;

/**
 */
public class PropertyCriteria extends AbstractCriteria {

    private String propertyName;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
