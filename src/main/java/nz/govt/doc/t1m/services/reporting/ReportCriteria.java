package nz.govt.doc.t1m.services.reporting;

import nz.govt.doc.t1m.domain.criteria.AbstractCriteria;
import nz.govt.doc.t1m.services.reporting.domain.Parameter;

import java.util.List;

/**
 */
public class ReportCriteria extends AbstractCriteria {

    private String name;
    private List<Parameter> parameterList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<Parameter> parameterList) {
        this.parameterList = parameterList;
    }
}
