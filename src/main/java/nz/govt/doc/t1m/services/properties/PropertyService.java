package nz.govt.doc.t1m.services.properties;

import com.mysema.query.collections.CollQuery;
import nz.govt.doc.t1m.domain.properties.PropertyEntity;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.domain.response.Response;
import nz.govt.doc.t1m.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.mysema.query.alias.Alias.*;
import static com.mysema.query.collections.CollQueryFactory.*;

/**
 * This is a lightweight property service that integrates with the Spring property value support and adds the ability
 * to update property values held in a database table. It also provides a pattern for defining property values.
 *
 * <ol>
 *  <li>Add the property name constant to the PropertyKey interface</li>
 *  <li>Add the property meta data to the PropertyMetaData class, use the MetaDataFactoryImpl to specify attributes like defaultValue, descriptions, visible, editable, hidden etc..</li>
 *  <li>If editable then add a "changeSet" to the Liquibase Change log to insert the value into the database</li>
 * </ol>
 *
 * <p>To use the property value in code use the standard Spring support for @Value.</p>
 *
 */
@Component
public class PropertyService {

    @Autowired
    protected PropertyRepository propertyRepository;

    @Autowired
    protected ConfigurableEnvironment configurableEnvironment;

    /**
     * Register our own property source with Spring so that we can get property values from the database.
     */
    @PostConstruct
    public void addPropertySources() {
        configurableEnvironment.getPropertySources().addLast(new PropertySource<PropertyService>("PropertyService", this) {
            @Override
            public Object getProperty(String name) {
                return getSource().getPropertyValueFromDB(name);
            }
        });
    }

    /**
     *
     * @param property
     * @return
     */
    private String getPropertyValue(PropertyMetaData property) {
        return getPropertyValue(property.getPropertyName());
    }

    /**
     * Application code should either use the standard Spring mechanisms like @Value, or call this method using the
     * constant value defined in the PropertyKeys class.
     *
     * @param name
     * @return
     */
    public String getPropertyValue(String name) {
        return configurableEnvironment.getProperty(name);
    }

    /**
     * This is what reads the property from the database - not expected to be called from outside of this package.
     *
     * @param name
     * @return
     */
    String getPropertyValueFromDB(String name) {
        PropertyEntity property = propertyRepository.findOneByName(name);

        String value = null;
        if(property != null) {
            value = property.getValue();
        }

        return value;
    }

    /**
     * Find method only returns the visible properties. Expected to be called from GUI.
     *
     * @param criteria
     * @return
     */
    public PagedResponse<PropertyModel> findByCriteria(PropertyCriteria criteria) {

        List<PropertyModel> allProperties = new ArrayList<>();
        for(PropertyMetaData nextKey : PropertyMetaData.values()) {
            String value = configurableEnvironment.getProperty(nextKey.getPropertyName());

            PropertyModel propertyModel = new PropertyModel();
            propertyModel.setName(nextKey.getPropertyName());
            propertyModel.setValue(value);
            propertyModel.setDefaultValue(nextKey.getDefaultValue());
            propertyModel.setDescription(nextKey.getDescription());
            propertyModel.setVisible(nextKey.isVisible());
            propertyModel.setEditable(nextKey.isEditable());
            propertyModel.setHidden(nextKey.isSecret());

            allProperties.add(propertyModel);
        }

        PropertyModel property = alias(PropertyModel.class);
        CollQuery query = from($(property), allProperties);

        // We only return "visible" properties for this method (no matter what) so that there is a code-level way
        // of hiding properties from the GUI
        query.where($(property.isVisible()).eq(true));

        String name = JPAUtils.appendWildcard(criteria.getPropertyName());
        if(name != null) {
            query.where($(property.getName()).like(name));
        }

        query.offset(criteria.getOffset()).limit(criteria.getPageSize());
        query.orderBy($(property.getName()).asc());
        PagedResponse<PropertyModel> response = JPAUtils.listResults(query, criteria, $(property));

        return response;
    }

    public PropertyModel findProperty(String propertyName) {
        PropertyMetaData propertyMetaData = findMetaData(propertyName);
        return findProperty(propertyMetaData);
    }

    public PropertyModel findProperty(PropertyMetaData propertyMetaData) {
        String value = configurableEnvironment.getProperty(propertyMetaData.getPropertyName());

        PropertyModel propertyModel = new PropertyModel();
        propertyModel.setName(propertyMetaData.getPropertyName());
        propertyModel.setValue(value);
        propertyModel.setDefaultValue(propertyMetaData.getDefaultValue());
        propertyModel.setDescription(propertyMetaData.getDescription());
        propertyModel.setVisible(propertyMetaData.isVisible());
        propertyModel.setEditable(propertyMetaData.isEditable());
        propertyModel.setHidden(propertyMetaData.isSecret());

        return propertyModel;
    }

    public PropertyMetaData findMetaData(String propertyName) {
        PropertyMetaData propertyMetaData = null;
        for(PropertyMetaData nextMetaData : PropertyMetaData.values()) {
            if(nextMetaData.getPropertyName().equals(propertyName)) {
                propertyMetaData = nextMetaData;
                break;
            }
        }

        if(propertyMetaData == null) {
            throw new RuntimeException("Unable to find property meta data for " + propertyName);
        }

        return propertyMetaData;
    }

    /**
     * Save the property value into our database table.
     *
     * @param propertyModel
     */
    @Transactional
    public Response<PropertyModel> saveProperty(PropertyModel propertyModel) {

        String propertyName = propertyModel.getName();
        String propertyValue = propertyModel.getValue();

        PropertyMetaData metaData = findMetaData(propertyName);

        if(!metaData.isEditable()) {
            throw new RuntimeException("Property " + propertyName + " is not editable.");
        }

        // gui is not allowed to save new properties - the property must already exist
        PropertyEntity propertyEntity = propertyRepository.findOneByName(propertyName);
        if(propertyEntity != null) {
            propertyEntity.setValue(propertyValue);
        }
        else {
            throw new RuntimeException("PropertyEntity must already exist before changes can be made.");
        }

        return new Response<>(propertyModel);
    }

}
