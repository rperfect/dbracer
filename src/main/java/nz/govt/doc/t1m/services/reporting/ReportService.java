package nz.govt.doc.t1m.services.reporting;

import nz.govt.doc.t1m.services.reporting.domain.Report;
import nz.govt.doc.t1m.utils.XmlUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
@Component
public class ReportService {

    @Autowired
    protected ReportDAO reportDAO;

    public Report getReport(String reportName) {
        return getReportMap().get(reportName);
    }

    public List<String> getReportNames() {
        List<String> reportNames = new ArrayList<>(getReportMap().keySet());
        return reportNames;
    }

    public Map<String, Report> getReportMap() {
        List<Report> reportList = loadReportDefinitions();
        Map<String, Report> reportMap = new HashMap<>();
        for(Report nextReport : reportList) {
            if(reportMap.containsKey(nextReport.getName())) {
                throw new RuntimeException("Report Map already has a report with " + nextReport.getName());
            }
            reportMap.put(nextReport.getName(), nextReport);
        }

        return reportMap;
    }

    public List<Report> loadReportDefinitions() {
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] reportListXml = resolver.getResources("classpath:/reports/**.xml");

            List<Report> reportList = new ArrayList<>();
            for (Resource nextResource : reportListXml) {
                String xml = IOUtils.toString(nextResource.getInputStream());
                Report report = XmlUtils.convertFromString(Report.class, xml);
                reportList.add(report);
            }

            return reportList;
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public DataTable executeReport(ReportCriteria reportCriteria) {
        Report report = getReport(reportCriteria.getName());
        if(report == null) {
            throw new RuntimeException("Unable to find report for " + reportCriteria.getName());
        }

        DataTable dataTable = reportDAO.executeReport(report, reportCriteria);
        return dataTable;
    }

}
