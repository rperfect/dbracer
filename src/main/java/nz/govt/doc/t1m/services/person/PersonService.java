package nz.govt.doc.t1m.services.person;

import com.bitbucket.thinbus.srp6.js.HexHashedXRoutine;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6CryptoParams;
import com.nimbusds.srp6.SRP6VerifierGenerator;
import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.domain.response.Response;
import nz.govt.doc.t1m.services.credentials.PersonCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigInteger;

/**
 */
@Component
public class PersonService {

    @Autowired
    protected PersonRepository personRepository;

    @Autowired
    protected PersonCredentialsService personCredentialsService;

    public PagedResponse<PersonEntity> findByCriteria(PersonCriteria criteria) {
        return personRepository.findByCriteria(criteria);
    }

    public Response<PersonEntity> findPersonById(Integer personId) {
        return new Response<>(personRepository.findOne(personId));
    }

    public Response<PersonEntity> findPersonByUsername(String username) {
        return new Response<>(personRepository.findOneByUsername(username));
    }

    @Transactional
    public Response<PersonEntity> savePerson(PersonEntity person, PersonCredentials personCredentials) {
        // validate the personEntity - is there already a person with this email address (with a different personId)?

        PersonEntity personResponse = personRepository.save(person);

        // We need personId from after the save because the id is assigned by the database and would be null for new persons otherwise.
        if(personCredentials != null && personCredentials.getSalt() != null && personCredentials.getVerifier() != null) {
            personCredentials.setPersonId(personResponse.getPersonId());
            personCredentialsService.saveCredentials(personCredentials);
        }

        return new Response<>(personResponse);
    }


    public void removePerson(Integer personId) {
        personRepository.delete(personId);
    }
}
