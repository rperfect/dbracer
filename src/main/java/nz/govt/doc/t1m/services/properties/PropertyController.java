package nz.govt.doc.t1m.services.properties;

import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.domain.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 */
@RestController
@RequestMapping("/rest/property")
public class PropertyController {

    @Autowired
    protected PropertyService propertyService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public PagedResponse<PropertyModel> search(@RequestBody PropertyCriteria criteria) {

//        if(true) {
//            throw new RuntimeException("Dummy Exception");
//        }

        return propertyService.findByCriteria(criteria);
    }

    @RequestMapping(value = "/{propertyName:.+}", method = RequestMethod.GET)
    public Response<PropertyModel> getProperty(@PathVariable(value = "propertyName") String propertyName) {
        return new Response<>(propertyService.findProperty(propertyName));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Response<PropertyModel> savePerson(@RequestBody Response<PropertyModel> request) {
        return propertyService.saveProperty(request.getModel());
    }
}
