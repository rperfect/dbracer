package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.services.audit.jpa.AuditEventRepository;

/**
 */
public interface AuditEventDAO  {

    PagedResponse<AuditEvent> findByCriteria(AuditEventCriteria criteria);

    AuditEvent saveAuditEvent(AuditEvent entity);

    AuditEvent findByRequestId(String s);

    void resetTable();
}
