package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import org.springframework.stereotype.Component;

import javax.xml.ws.handler.MessageContext;
import java.util.List;
import java.util.Random;

/**
 */
@Component
public class MeasurementActionRequestId implements MeasurementAction {

    @Override
    public void performMeasurement(MeasurementContext measurementCtx) {
        measurementCtx.startTimer();

        for(int i = 0; i < measurementCtx.getSampleSize(); i++) {

            int randomIdx = new Random().nextInt(measurementCtx.getSampleList().size());
            AuditEvent auditEvent = measurementCtx.getSampleList().get(randomIdx);

            AuditEvent auditEventFromDB = measurementCtx.getAuditEventDAO().findByRequestId(auditEvent.getRequestId());

            int percentComplete = Math.round((float)i / measurementCtx.getSampleSize() * 100);
            measurementCtx.setStatus(measurementCtx.getMeasurement().getName() + ": " + getClass().getSimpleName() + "-" + percentComplete + "%");
        }

        measurementCtx.getMeasurement().setRequestId(measurementCtx.endTimerAndGetSampleDuration());
    }
}
