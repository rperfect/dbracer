package nz.govt.doc.t1m.services.audit.jpa;

import com.mysema.query.jpa.impl.JPAQuery;
import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.audit.QAuditEvent;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.services.audit.AuditEventCriteria;
import nz.govt.doc.t1m.utils.JPAUtils;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 */
@Component
public class AuditEventRepositoryImpl implements AuditEventRepositoryCustom {

    @PersistenceContext
    protected EntityManager em;

    @Override
    @Transactional
    public AuditEvent saveAuditEvent(AuditEvent entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public AuditEvent findByRequestId(String requestId) {
        QAuditEvent auditEvent = QAuditEvent.auditEvent;
        JPAQuery query = new JPAQuery(em).from(auditEvent);
        query.where(auditEvent.requestId.eq(requestId));
        AuditEvent result = query.uniqueResult(auditEvent);
        return result;
    }

    @Override
    public PagedResponse<AuditEvent> findByCriteria(AuditEventCriteria criteria) {


        QAuditEvent auditEvent = QAuditEvent.auditEvent;
        JPAQuery query = new JPAQuery(em).from(auditEvent);

        if(criteria.getUserId() != null) {
            query.where(auditEvent.userId.eq(criteria.getUserId()));
        }

        if(criteria.getEventType() != null) {
            query.where(auditEvent.eventType.eq(criteria.getEventType()));
        }

        if(criteria.getFromEventTime() != null) {
            query.where(auditEvent.eventTime.gt(criteria.getFromEventTime()));
        }

        if(criteria.getToEventTime() != null) {
            query.where(auditEvent.eventTime.lt(criteria.getToEventTime()));
        }

        query.orderBy(auditEvent.eventTime.desc());

        PagedResponse<AuditEvent> response = JPAUtils.listResults(query, criteria, auditEvent);
        return response;
    }

    @Override
    public void resetTable() {
        throw new RuntimeException("TODO");
    }
}
