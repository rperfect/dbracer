package nz.govt.doc.t1m.services.reporting.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Report {

    private String name;

    @XmlElement(name = "parameter")
    @XmlElementWrapper(name = "parameterList")
    private List<Parameter> parameterList;

    private Query query;
    private View view;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<Parameter> parameterList) {
        this.parameterList = parameterList;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}
