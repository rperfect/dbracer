package nz.govt.doc.t1m.services.reporting;

import nz.govt.doc.t1m.domain.response.Response;
import nz.govt.doc.t1m.services.reporting.domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@RestController
@RequestMapping("/rest/report")
public class ReportController {

    @Autowired
    protected ReportService reportService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<String> listReportNames() {
        return reportService.getReportNames();
    }

    @RequestMapping(value = "/{reportName}", method = RequestMethod.GET)
    public Response<Report> getReport(@PathVariable(value = "reportName") String reportName) {
        return new Response<>(reportService.getReport(reportName));
    }

    @RequestMapping(value = "/execute", method = RequestMethod.POST)
    @ResponseBody
    public DataTable executeReport(@RequestBody ReportCriteria reportCriteria) {

        DataTable dataTable = reportService.executeReport(reportCriteria);

        return dataTable;
    }
}
