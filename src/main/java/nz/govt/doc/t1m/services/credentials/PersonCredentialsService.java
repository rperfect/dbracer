package nz.govt.doc.t1m.services.credentials;

import com.bitbucket.thinbus.srp6.js.HexHashedXRoutine;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6CryptoParams;
import com.nimbusds.srp6.SRP6VerifierGenerator;
import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 */
@Component
public class PersonCredentialsService {

    @Autowired
    protected PersonCredentialsRepository personCredentialsRepository;

    public PersonCredentials findByPersonId(Integer personId) {
        return personCredentialsRepository.findOneByPersonId(personId);
    }

    /**
     * As part of saving a person this method will be called to save the password. If the password is specified then
     * it is converted into a hash value and saved in a separate PersonCredentials object for security reasons so there
     * is less chance that database password hash values can be exposed to the web tier.
     *
     */
    @Transactional
    public void saveCredentials(PersonCredentials personCredentials) {

        if(personCredentials.getPersonCredentialsId() != null) {
            PersonCredentials personCredentialsFromDB = personCredentialsRepository.findOneByPersonId(personCredentials.getPersonId());
            if(personCredentialsFromDB != null) {
                // Credentials object already saved in database, so update the property. With Hibernate in this case
                // since the object was read within a transaction we don't actually need to call save() on the object
                // since it is already attached to the transaction when it was read.
                personCredentialsFromDB.setSalt(personCredentials.getSalt());
                personCredentialsFromDB.setVerifier(personCredentials.getVerifier());
            }
            else {
                personCredentialsRepository.save(personCredentials);
            }
        }
        else {
            personCredentialsRepository.save(personCredentials);
        }
    }


    private static SRP6CryptoParams getSrp6CryptoParams() {
        BigInteger n = new BigInteger("21766174458617435773191008891802753781907668374255538511144643224689886235383840957210909013086056401571399717235807266581649606472148410291413364152197364477180887395655483738115072677402235101762521901569820740293149529620419333266262073471054548368736039519702486226506248861060256971802984953561121442680157668000761429988222457090413873973970171927093992114751765168063614761119615476233422096442783117971236371647333871414335895773474667308967050807005509320424799678417036867928316761272274230314067548291133582479583061439577559347101961771406173684378522703483495337037655006751328447510550299250924469288819");
        BigInteger g = new BigInteger("2");
        String h = "SHA-256";
        SRP6CryptoParams cryptoParams = new SRP6CryptoParams(n, g, h);
        return cryptoParams;
    }

    public PersonCredentials createPersonCredentials(String username, String password) {

        SRP6CryptoParams cryptoParams = getSrp6CryptoParams();

        // Create verifier generator
        SRP6VerifierGenerator gen = new SRP6VerifierGenerator(cryptoParams);
        gen.setXRoutine(new HexHashedXRoutine());

        // Generate new salt and verifier
        BigInteger salt = new BigInteger(SRP6VerifierGenerator.generateRandomSalt(32));
        BigInteger verifier = gen.generateVerifier(salt, username, password);

        String saltHex = BigIntegerUtils.toHex(salt);
        String verifierHex = BigIntegerUtils.toHex(verifier);

        PersonCredentials personCredentials = new PersonCredentials();
        personCredentials.setSalt(saltHex);
        personCredentials.setVerifier(verifierHex);
        return personCredentials;
    }

}
