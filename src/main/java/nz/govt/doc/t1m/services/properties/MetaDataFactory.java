package nz.govt.doc.t1m.services.properties;

/**
 */
interface MetaDataFactory {

    MetaDataFactory visible();
    MetaDataFactory editable();
    MetaDataFactory hidden();
}
