package nz.govt.doc.t1m.services.audit.dynamodb;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.services.audit.AuditEventCriteria;
import nz.govt.doc.t1m.services.audit.AuditEventDAO;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 */
@Component
public class AuditEventDynoDAO implements AuditEventDAO {

    private boolean auditEventTableExists;

    private AmazonDynamoDBClient client;
    private DynamoDB dynamoDB;
    private DynamoDBMapper mapper;

    private synchronized AmazonDynamoDBClient getAmazonDynamoDBClient() {
        if(client == null) {
            //AmazonDynamoDBClient client = new AmazonDynamoDBClient().withEndpoint("http://localhost:8000");
            //client.setSignerRegionOverride("ap-southeast-2");

            client = new AmazonDynamoDBClient();
            client.setRegion(Region.getRegion(Regions.US_WEST_2));
        }
        return client;
    }

    private synchronized DynamoDB getDynamoDB() {
        if(dynamoDB == null) {
            dynamoDB = new DynamoDB(getAmazonDynamoDBClient());
        }
        return dynamoDB;
    }

    private synchronized DynamoDBMapper getDynamoDBMapper() {
        if(mapper == null) {
            mapper = new DynamoDBMapper(getAmazonDynamoDBClient());
        }
        return mapper;
    }

    @Override
    public void resetTable() {
        deleteTable();
        createTable();
    }

    public void deleteTable() {
        try {
            DynamoDB dynamoDB = getDynamoDB();

            Table table = dynamoDB.getTable("ProductCatalog");

            if (table != null) {
                table.delete();
                table.waitForDelete();
            }
        }
        catch (InterruptedException ex) {
            // ignore
        }
    }

    public void createTable() {

        try {
            DynamoDB dynamoDB = getDynamoDB();
            System.out.println("Attempting to create table; please wait...");

            DynamoDBMapper mapper = getDynamoDBMapper();
            CreateTableRequest createTableRequest = mapper.generateCreateTableRequest(AuditEvent.class)
                    .withProvisionedThroughput(new ProvisionedThroughput(10L, 1L));

            for(GlobalSecondaryIndex nextIndex : createTableRequest.getGlobalSecondaryIndexes()) {
                nextIndex.setProvisionedThroughput(new ProvisionedThroughput(10L, 1L));
            }

            Table table = dynamoDB.createTable(createTableRequest);

            table.waitForActive();
            System.out.println("Success.  Table status: " + table.getDescription().getTableStatus());
            System.out.println("Success.  Table description: " + table.getDescription());
        }
        catch (Exception e) {
            System.err.println("Unable to create table: ");
            System.err.println(e.getMessage());
        }
    }

    private void checkTableExists() {
        if(!auditEventTableExists) {
            DynamoDB dynamoDB = getDynamoDB();

            TableCollection<ListTablesResult> tables = dynamoDB.listTables();
            Iterator<Table> iterator = tables.iterator();

            while (iterator.hasNext() && !auditEventTableExists) {
                Table table = iterator.next();
                if (table.getTableName().equals("AuditEvent")) {
                    auditEventTableExists = true;
                    System.out.println(table.describe());
                }
            }

            if (!auditEventTableExists) {
                createTable();
            }
        }
    }

    @Override
    public PagedResponse<AuditEvent> findByCriteria(AuditEventCriteria criteria) {
        Map<String, AttributeValue> eav = new HashMap<>();

        PagedResponse<AuditEvent> response = new PagedResponse<>();
        String keyConditionExpression = null;
        String indexName = null;
        if(criteria.getUserId() != null) {
            indexName = "userIdIndex";
            eav.put(":userId", new AttributeValue(criteria.getUserId()));
            keyConditionExpression = "userId = :userId";
        }
        else if(criteria.getEventType() != null) {
            indexName = "eventTypeIndex";
            eav.put(":eventType", new AttributeValue(criteria.getEventType()));
            keyConditionExpression = "eventType = :eventType";
        }
        else if(criteria.getFromEventTime() != null && criteria.getToEventTime() != null) {

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            String fromTimeStr = dateFormatter.format(criteria.getFromEventTime());
            String toTimeStr = dateFormatter.format(criteria.getToEventTime());

            indexName = "eventTimeIndex";
            eav.put(":eventTimeHash", new AttributeValue(AuditEvent.EVENT_TIME_HASH));
            eav.put(":fromEventTime", new AttributeValue(fromTimeStr));
            eav.put(":toEventTime", new AttributeValue(toTimeStr));
            keyConditionExpression = "eventTimeHash = :eventTimeHash and eventTime between :fromEventTime and :toEventTime";
        }
        else {
            System.out.println("WARNING: unimplemented query for the the supplied criteria - work todo here :-)");
        }


        if(keyConditionExpression != null) {
            DynamoDBQueryExpression<AuditEvent> queryExpression = new DynamoDBQueryExpression<AuditEvent>()
                    .withIndexName(indexName)
                    .withKeyConditionExpression(keyConditionExpression)
                    .withConsistentRead(false)
                    .withExpressionAttributeValues(eav);

            DynamoDBMapper mapper = getDynamoDBMapper();
            PaginatedQueryList<AuditEvent> paginatedQueryList = mapper.query(AuditEvent.class, queryExpression);
            Iterator<AuditEvent> iterator = paginatedQueryList.iterator();
            for (int i = 0; i < criteria.getPageSize() && iterator.hasNext(); i++) {
                response.getResults().add(iterator.next());
            }
        }

        return response;
    }

    @Override
    public AuditEvent saveAuditEvent(AuditEvent entity) {
        checkTableExists();

        DynamoDBMapper mapper = getDynamoDBMapper();
        mapper.save(entity);

        return entity;
    }

    @Override
    public AuditEvent findByRequestId(String requestId) {
        DynamoDBMapper mapper = new DynamoDBMapper(getAmazonDynamoDBClient());
        AuditEvent auditEvent = mapper.load(AuditEvent.class, requestId, null);
        return auditEvent;
    }
}
