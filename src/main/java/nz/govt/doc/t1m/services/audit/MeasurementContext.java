package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.audit.Measurement;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class MeasurementContext {

    private MeasurementCriteria measurementCriteria;
    private Measurement measurement;
    private AuditEventDAO auditEventDAO;
    private AuditEventFactory auditEventFactory;

    private List<AuditEvent> sampleList;
    private long timer;
    private String status;

    public MeasurementContext(MeasurementCriteria measurementCriteria, AuditEventDAO auditEventDAO, AuditEventFactory auditEventFactory) {
        this.measurementCriteria = measurementCriteria;
        this.auditEventDAO = auditEventDAO;
        this.auditEventFactory = auditEventFactory;
    }

    public void createNextMeasurement(int iteration) {
        measurement = new Measurement(measurementCriteria, iteration);
        sampleList = new ArrayList<>();
    }

    public AuditEventFactory getAuditEventFactory() {
        return auditEventFactory;
    }

    public int getIterationSize() {
        return measurementCriteria.getIterationSize();
    }

    public int getIterationCount() {
        return measurementCriteria.getIterationCount();
    }

    public int getSampleSize() {
        return measurement.getSampleSize();
    }

    public void setSampleSize(int sampleSize) {
        measurement.setSampleSize(sampleSize);
    }

    public List<AuditEvent> getSampleList() {
        return sampleList;
    }

    public void setSampleList(List<AuditEvent> sampleList) {
        this.sampleList = sampleList;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public AuditEventDAO getAuditEventDAO() {
        return auditEventDAO;
    }

    public void setAuditEventDAO(AuditEventDAO auditEventDAO) {
        this.auditEventDAO = auditEventDAO;
    }

    public void startTimer() {
        timer = System.currentTimeMillis();
    }

    public long endTimerAndGetTotalDuration() {
        long endTime = System.currentTimeMillis();
        long totalDuration = endTime - timer;
        timer = 0;
        return totalDuration;
    }

    public float endTimerAndGetSampleDuration() {
        float sampleDuration = (float)endTimerAndGetTotalDuration() / measurement.getSampleSize();
        return sampleDuration;
    }

    public synchronized String getStatus() {
        return status;
    }

    public synchronized void setStatus(String status) {
        this.status = status;
    }
}
