package nz.govt.doc.t1m.services.reporting;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class DataRow {

    private List<DataCell> cells;

    public List<DataCell> getCells() {
        if (cells == null) {
            cells = new ArrayList<DataCell>();
        }
        return cells;
    }

    public void setCells(List<DataCell> cells) {
        this.cells = cells;
    }
}
