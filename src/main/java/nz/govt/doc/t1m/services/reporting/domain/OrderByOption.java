package nz.govt.doc.t1m.services.reporting.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderByOption {
}
