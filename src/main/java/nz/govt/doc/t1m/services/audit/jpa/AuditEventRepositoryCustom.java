package nz.govt.doc.t1m.services.audit.jpa;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import nz.govt.doc.t1m.domain.response.PagedResponse;
import nz.govt.doc.t1m.services.audit.AuditEventCriteria;
import nz.govt.doc.t1m.services.audit.AuditEventDAO;

/**
 */
public interface AuditEventRepositoryCustom extends AuditEventDAO {

    PagedResponse<AuditEvent> findByCriteria(AuditEventCriteria criteria);

}
