package nz.govt.doc.t1m.services.reporting.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Query {

    private String datasource;
    private String select;
    private String fromWhere;
    private List<OrderByOption> orderByOptionList;

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getFromWhere() {
        return fromWhere;
    }

    public void setFromWhere(String fromWhere) {
        this.fromWhere = fromWhere;
    }

    public List<OrderByOption> getOrderByOptionList() {
        return orderByOptionList;
    }

    public void setOrderByOptionList(List<OrderByOption> orderByOptionList) {
        this.orderByOptionList = orderByOptionList;
    }
}
