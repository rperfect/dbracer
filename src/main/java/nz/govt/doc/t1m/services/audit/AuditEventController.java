package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.Measurement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 */
@RestController
@RequestMapping("/rest/audit")
public class AuditEventController {

    @Autowired
    protected AuditEventService auditEventService;


    @RequestMapping(value = "/performMeasurements", method = RequestMethod.POST)
    @ResponseBody
    public void performMeasurements(@RequestBody MeasurementCriteria measurementCriteria) {
        auditEventService.performMeasurements(measurementCriteria);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getMeasurementStatus() {
        return auditEventService.getMeasurementStatus();
    }
}
