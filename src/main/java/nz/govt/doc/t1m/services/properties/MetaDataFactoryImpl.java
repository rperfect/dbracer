package nz.govt.doc.t1m.services.properties;

class MetaDataFactoryImpl implements MetaDataFactory {

    private String name;
    private String defaultValue;
    private String description;
    private boolean visible;
    private boolean editable;
    private boolean secret;

    static MetaDataFactory create(String name, String defaultValue, String description) {
        return new MetaDataFactoryImpl(name, defaultValue, description);
    }

    private MetaDataFactoryImpl(String name, String defaultValue, String description) {
        this.defaultValue = defaultValue;
        this.description = description;
        this.name = name;
    }

    @Override
    public MetaDataFactory visible() {
        this.visible = true;
        return this;
    }

    @Override
    public MetaDataFactory editable() {
        this.editable = true;
        return this;
    }

    @Override
    public MetaDataFactory hidden() {
        this.secret = true;
        return this;
    }


    String getName() {
        return name;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getDescription() {
        return description;
    }

    boolean isVisible() {
        return visible;
    }

    boolean isEditable() {
        return editable;
    }

    boolean isSecret() {
        return secret;
    }
}