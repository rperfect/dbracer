package nz.govt.doc.t1m.services.credentials;

import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSessionSHA256;
import com.nimbusds.srp6.SRP6CryptoParams;
import com.nimbusds.srp6.SRP6ServerSession;
import nz.govt.doc.t1m.domain.person.PersonCredentials;
import nz.govt.doc.t1m.domain.person.PersonEntity;
import nz.govt.doc.t1m.services.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.HashMap;

/**
 */
@Component
public class ThinbusSRPService {

    @Autowired
    protected PersonService personService;

    @Autowired
    protected PersonCredentialsService personCredentialsService;

    @Autowired
    protected JwtTokenService tokenService;

    @Autowired
    protected UserDetailsService userDetailsService;


    private HashMap<String, SRP6JavascriptServerSession> serverSessionMap = new HashMap<>();


    public PersonCredentials getUserCredentials(String username) {
        PersonCredentials personCredentials = null;
        PersonEntity personEntity = personService.findPersonByUsername(username).getModel();
        if(personEntity != null) {
            personCredentials = personCredentialsService.findByPersonId(personEntity.getPersonId());
        }

        return personCredentials;
    }

    /**
     * Just sticking the server sessions in a HashMap for now. Although they're only stored here for as long as it takes
     * to complete the authentication, if there were 1000's of broken authentications this would cause a memory leak. But
     * it would be an easy one to find.
     *
     * @param requestId
     * @param serverSession
     */
    private void saveServerSession(String requestId, SRP6JavascriptServerSession serverSession) {
        if(requestId == null) {
            throw new NullPointerException();
        }

        if(serverSession == null) {
            throw new NullPointerException();
        }

        if(serverSessionMap.containsKey(requestId)) {
            throw new RuntimeException("Server session already exists");
        }

        serverSessionMap.put(requestId, serverSession);
    }

    private SRP6JavascriptServerSession loadServerSession(String requestId) {
        if(requestId == null) {
            throw new NullPointerException();
        }

        SRP6JavascriptServerSession serverSession = serverSessionMap.get(requestId);
        if(serverSession == null) {
            throw new RuntimeException("Server session not found");
        }

        return serverSession;
    }

    private void deleteServerSession(String requestId) {
        if(requestId == null) {
            throw new NullPointerException();
        }

        serverSessionMap.remove(requestId);
    }



    public Step1Data serverStep1(Step1Data step1Data) {

        final String username = step1Data.getUsername();

        //SRP6CryptoParams config = SRP6CryptoParams.getInstance();
        BigInteger n = new BigInteger("21766174458617435773191008891802753781907668374255538511144643224689886235383840957210909013086056401571399717235807266581649606472148410291413364152197364477180887395655483738115072677402235101762521901569820740293149529620419333266262073471054548368736039519702486226506248861060256971802984953561121442680157668000761429988222457090413873973970171927093992114751765168063614761119615476233422096442783117971236371647333871414335895773474667308967050807005509320424799678417036867928316761272274230314067548291133582479583061439577559347101961771406173684378522703483495337037655006751328447510550299250924469288819");
        BigInteger g = new BigInteger("2");
        String h = "SHA-256";
        SRP6CryptoParams config = new SRP6CryptoParams(n, g, h);
        SRP6JavascriptServerSession serverSession = new SRP6JavascriptServerSessionSHA256(config.N.toString(), config.g.toString());

        // Retrieve user verifier 'v' + salt 's' from database
        PersonCredentials personCredentials = getUserCredentials(username);
        String salt = personCredentials.getSalt();
        String verifier = personCredentials.getVerifier();

        // Compute the public server value 'B'
        String b = serverSession.step1(username, salt, verifier);

        // Persist this authentication request for subsequent step
        String requestId = step1Data.getRequestId();
        saveServerSession(requestId, serverSession);

        step1Data.setSalt(salt);
        step1Data.setB(b);
        return step1Data;
    }

    public Step2Data serverStep2(Step2Data step2Data) {

        String a = step2Data.getA();
        String m1 = step2Data.getM1();
        String m2 = null;
        String jwtToken = null;

        String requestId = step2Data.getRequestId();
        try {
            SRP6JavascriptServerSession serverSession = loadServerSession(requestId);
            m2 = serverSession.step2(a, m1);

            // If we get here, then the server thinks that authentication is all good.
            String userID = serverSession.getUserID();
            UserDetails userDetails = userDetailsService.loadUserByUsername(userID);
            jwtToken = tokenService.generateToken(userDetails);

            System.out.println("Step2: userID = " + serverSession.getUserID() + " - " + serverSession.getSessionKey(true));
        }
        catch (Exception e) {
            // User authentication failed
            e.printStackTrace();
            throw new BadCredentialsException("Invalid Credentials", e);
        }
        finally {
            deleteServerSession(requestId);
        }

        step2Data.setM2(m2);
        step2Data.setJwtToken(jwtToken);
        return step2Data;
    }



}
