package nz.govt.doc.t1m.services.audit;

import nz.govt.doc.t1m.domain.audit.AuditEvent;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Date;
import java.util.UUID;

/**
 *
 */
@Component
public class AuditEventFactory {

    private static final int  USER_POPULATION_SIZE = 1000000;
    private static final long EVENTS_PER_DAY = 1000000;
    private static final long SECONDS_PER_DAY = 24 * 60 * 60;
    private static final int  EVENT_TYPES_COUNT = 150;

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private DateTime dateTimeFactory = new DateTime(2000, 1, 1, 1, 1);
    private DateTime firstDateTime = dateTimeFactory;
    private DateTime lastDateTime = dateTimeFactory;
    private long eventCounter = 0;
    private SecureRandom random = new SecureRandom();
    private String userId;

    private String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) {
            sb.append(AB.charAt(random.nextInt(AB.length())));
        }

        return sb.toString();
    }

    public DateTime[] getRandomDateTimeRange(long periodMins) {
        Duration duration = new Duration(firstDateTime, lastDateTime);
        if(periodMins > duration.getStandardMinutes()) {
            periodMins = duration.getStandardMinutes();
        }

        if(periodMins < 1) {
            periodMins = 1;
        }

        DateTime randomEnd = lastDateTime.minusMinutes((int)periodMins);
        long randomRangeInMins = new Duration(firstDateTime, randomEnd).getStandardMinutes();

        int randomStartMins;
        if(randomRangeInMins <= 1) {
            randomStartMins = 0;
        }
        else {
            randomStartMins = random.nextInt((int) randomRangeInMins);
        }


        DateTime startDateTime = firstDateTime.plusMinutes(randomStartMins);
        DateTime endDateTime = startDateTime.plusMinutes((int)periodMins);

        DateTime[] randomDateTimeRange = new DateTime[2];
        randomDateTimeRange[0] = startDateTime;
        randomDateTimeRange[1] = endDateTime;

        return randomDateTimeRange;
    }


    public AuditEvent createNext() {
        eventCounter++;

        long eventsPerSecond = EVENTS_PER_DAY / SECONDS_PER_DAY;
        if(eventCounter % eventsPerSecond == 0) {
            dateTimeFactory = dateTimeFactory.plusSeconds(1);
            lastDateTime = dateTimeFactory;
        }

        String requestId = UUID.randomUUID().toString();
        Date eventTime = dateTimeFactory.toDate();
        String userId = createUserId();
        String eventType = createEventType();
        String eventData = randomString(100 + random.nextInt(150) + random.nextInt(150));
        long duration = random.nextInt(50) + random.nextInt(50);

        AuditEvent auditEvent = new AuditEvent();
        auditEvent.setRequestId(requestId);
        auditEvent.setEventTime(eventTime);
        auditEvent.setUserId(userId);
        auditEvent.setEventType(eventType);
        auditEvent.setEventData(eventData);
        auditEvent.setDuration(duration);

        return auditEvent;
    }

    public String createUserId() {
        if(userId == null || random.nextInt(100) < 10) {
            userId = "User" + random.nextInt(USER_POPULATION_SIZE);
        }
        return userId;
    }

    public String createEventType() {
        return "EVT" + random.nextInt(EVENT_TYPES_COUNT);
    }
}
