package nz.govt.doc.t1m.services.audit;

/**
 */
public class MeasurementCriteria {

    private String name;
    private int iterationSize = 1000;
    private int iterationCount = 3;
    private int sampleSize = 100;
    private String storageType = "DynamoDB";  // MySQL, Auroa, SQLServer
    private boolean resetTable = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIterationSize() {
        return iterationSize;
    }

    public void setIterationSize(int iterationSize) {
        this.iterationSize = iterationSize;
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public void setIterationCount(int iterationCount) {
        this.iterationCount = iterationCount;
    }

    public int getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public boolean isResetTable() {
        return resetTable;
    }

    public void setResetTable(boolean resetTable) {
        this.resetTable = resetTable;
    }
}
