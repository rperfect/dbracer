package nz.govt.doc.t1m.services.person;

import nz.govt.doc.t1m.domain.criteria.AbstractCriteria;

/**
 */
public class PersonCriteria extends AbstractCriteria {

    private String nameCriteria;


    public String getNameCriteria() {
        return nameCriteria;
    }

    public void setNameCriteria(String nameCriteria) {
        this.nameCriteria = nameCriteria;
    }
}
