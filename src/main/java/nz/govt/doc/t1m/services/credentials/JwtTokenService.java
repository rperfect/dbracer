package nz.govt.doc.t1m.services.credentials;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.method.P;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class JwtTokenService {

    @Value("${jwt.token.secret}")
    private String secret;

    @Value("${jwt.token.expirationInSecs}")
    private long expirationInSecs;

    private static final String CLAIM_KEY_AUTHORITIES = "authorities";


    private String getAuthoritiesAsCSV(Collection<? extends GrantedAuthority> authoritiesCollection) {
        return StringUtils.collectionToCommaDelimitedString(authoritiesCollection);
    }

    private Collection<GrantedAuthority> getAuthoritiesAsCollection(String authoritiesCSV) {
        String[] authoritiesArray = StringUtils.commaDelimitedListToStringArray(authoritiesCSV);
        Collection<GrantedAuthority> authoritiesCollection = new ArrayList<>();
        for(String nextAuthority : authoritiesArray) {
            authoritiesCollection.add(new SimpleGrantedAuthority(nextAuthority));
        }

        return  authoritiesCollection;
    }

    public UserDetails parseToken(String token) {
        Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);

        Date expirationDate = claims.getBody().getExpiration();
        if(expirationDate.before(new Date())) {
            throw new SessionAuthenticationException("Token expired");
        }

        String principal = claims.getBody().getSubject();
        String credentials = "XXX";  // we don't actually know the credentials, and we'll erase them as well below...
        String authoritiesCSV = claims.getBody().get(CLAIM_KEY_AUTHORITIES, String.class);
        Collection<GrantedAuthority> authoritiesCollection = getAuthoritiesAsCollection(authoritiesCSV);

        User user = new User(principal, credentials, authoritiesCollection);
        user.eraseCredentials();
        return user;
    }

    public String generateToken(UserDetails userDetails) {
        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + expirationInSecs * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .claim(CLAIM_KEY_AUTHORITIES, getAuthoritiesAsCSV(userDetails.getAuthorities()))
                .compact();
    }

}