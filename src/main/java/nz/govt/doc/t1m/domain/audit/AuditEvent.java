package nz.govt.doc.t1m.domain.audit;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

import javax.persistence.*;
import java.util.Date;

/**
 */
@Entity
@Table(name = "AuditEvent",
        indexes = {@Index(name = "idx_eventTime", columnList = "eventTime"), @Index(name = "idx_userId", columnList = "userId"), @Index(name = "idx_eventType", columnList = "eventType")}
)
@DynamoDBTable(tableName="AuditEvent")
public class AuditEvent {

    // This is a constant value so that we can index by eventTime
    public static final String EVENT_TIME_HASH = "STATIC";

    @Id
    private String requestId;

    private String eventTimeHash = EVENT_TIME_HASH;
    private Date eventTime;
    private String userId;
    private String eventType;

    @Column(length = 500)
    private String eventData;
    private long duration;

    public AuditEvent() {
    }

    @DynamoDBHashKey(attributeName="requestId")
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @DynamoDBIndexHashKey(globalSecondaryIndexName = "eventTimeIndex")
    public String getEventTimeHash() {
        return eventTimeHash;
    }

    public void setEventTimeHash(String eventTimeHash) {
        this.eventTimeHash = eventTimeHash;
    }

    @DynamoDBIndexRangeKey(globalSecondaryIndexNames = {"userIdIndex", "eventTypeIndex", "eventTimeIndex"})
    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    @DynamoDBIndexHashKey(globalSecondaryIndexName = "userIdIndex")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @DynamoDBIndexHashKey(globalSecondaryIndexName = "eventTypeIndex")
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
