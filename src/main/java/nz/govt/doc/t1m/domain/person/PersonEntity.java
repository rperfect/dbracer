package nz.govt.doc.t1m.domain.person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 */
@Entity
@Table(name = "Person", uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer personId;

    @NotNull
    private String username;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PersonGroup personGroup;

    private String familyName;
    private String firstName;


    @Transient
    private String passwordEdit;
    @Transient
    private String passwordConfirm;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PersonGroup getPersonGroup() {
        return personGroup;
    }

    public void setPersonGroup(PersonGroup personGroup) {
        this.personGroup = personGroup;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPasswordEdit() {
        return passwordEdit;
    }

    public void setPasswordEdit(String passwordEdit) {
        this.passwordEdit = passwordEdit;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
