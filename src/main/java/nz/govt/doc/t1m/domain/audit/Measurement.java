package nz.govt.doc.t1m.domain.audit;

import nz.govt.doc.t1m.services.audit.MeasurementCriteria;

import javax.persistence.*;

/**
 */
@Entity
@Table(name = "Measurement")
public class Measurement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer measurementId;

    private String name;
    private String storageType;
    private int iterationSize;
    private int iterationCount;
    private int sampleSize;
    private long createAllEvents;
    private float createOneEvent;
    private float requestId;
    private float dateRange;
    private float userId;
    private float eventType;

    public Measurement(MeasurementCriteria measurementCriteria, int iteration) {
        this.name = measurementCriteria.getName() + "." + iteration;
        this.storageType = measurementCriteria.getStorageType();
        this.iterationSize = measurementCriteria.getIterationSize();
        this.iterationCount = measurementCriteria.getIterationCount();
        this.sampleSize = measurementCriteria.getSampleSize();
    }

    public Integer getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(Integer measurementId) {
        this.measurementId = measurementId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public int getIterationSize() {
        return iterationSize;
    }

    public void setIterationSize(int iterationSize) {
        this.iterationSize = iterationSize;
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public void setIterationCount(int iterationCount) {
        this.iterationCount = iterationCount;
    }

    public int getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    public long getCreateAllEvents() {
        return createAllEvents;
    }

    public void setCreateAllEvents(long createAllEvents) {
        this.createAllEvents = createAllEvents;
    }

    public float getCreateOneEvent() {
        return createOneEvent;
    }

    public void setCreateOneEvent(float createOneEvent) {
        this.createOneEvent = createOneEvent;
    }

    public float getRequestId() {
        return requestId;
    }

    public void setRequestId(float requestId) {
        this.requestId = requestId;
    }

    public float getDateRange() {
        return dateRange;
    }

    public void setDateRange(float dateRange) {
        this.dateRange = dateRange;
    }

    public float getUserId() {
        return userId;
    }

    public void setUserId(float userId) {
        this.userId = userId;
    }

    public float getEventType() {
        return eventType;
    }

    public void setEventType(float eventType) {
        this.eventType = eventType;
    }
}
