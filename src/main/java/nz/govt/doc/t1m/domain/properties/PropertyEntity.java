package nz.govt.doc.t1m.domain.properties;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 */
@Entity
public class PropertyEntity {

    @Id
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    // helper getters
    public Integer getValueAsInteger() {
        return  null;
    }

    public Boolean getValueAsBoolean() {
        return null;
    }

    public Double getValueAsDouble() {
        return null;
    }

    public Date getValueAsDate() {
        return null;
    }
}
