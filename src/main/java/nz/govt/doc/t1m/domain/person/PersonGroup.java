package nz.govt.doc.t1m.domain.person;

/**
 */
public enum PersonGroup {

    SYS_ADMIN,
    OPERATOR,
    FINANCE,
    CC_TEAM_LEAD,
    CC_OPERATOR;

}
