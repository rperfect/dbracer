package nz.govt.doc.t1m.domain.person;

import javax.persistence.*;

/**
 */
@Entity
public class PersonCredentials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer personCredentialsId;
    private Integer personId;
    private String salt;

    @Column(length = 600)
    private String verifier;

    public Integer getPersonCredentialsId() {
        return personCredentialsId;
    }

    public void setPersonCredentialsId(Integer personCredentialsId) {
        this.personCredentialsId = personCredentialsId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getVerifier() {
        return verifier;
    }

    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }
}
