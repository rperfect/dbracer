


define spring_boot_service(
  String $sourceFile,
) {
  file { "/opt/${title}":
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { "/opt/${title}/${title}.jar":
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "${sourceFile}",
  }

  file { "/opt/${title}/application.properties":
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => "
logging.file=${title}.log
spring.datasource.url=jdbc:mysql://dbracer.czv2agzfgsnr.us-west-2.rds.amazonaws.com:3306/dbracer
spring.datasource.username=dbracerAdmin
spring.datasource.password=TheDogWentDownToTheWater
",
  }

  file { "/etc/systemd/system/${title}.service":
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    content => "
[Unit]
Description=${title}
After=syslog.target

[Service]
User=root
ExecStart=/opt/${title}/${title}.jar
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
",
  }

  service { "${title}.service":
    ensure => 'running',
    enable => true,
  }
}


class dbracer_service{
  spring_boot_service { 'dbracer':
    sourceFile => 'http://rjp-deployments.s3-website-ap-southeast-2.amazonaws.com/dbracer-0.1.0.jar',
  }
}

include dbracer_service
